//
//  ViewController.swift
//  SwiftyJson
//
//  Created by PRIYA GONGAL on 29/07/19.
//  Copyright © 2019 Priya Gongal. All rights reserved.
//

import UIKit
import Kingfisher
class ViewController: UIViewController {
    @IBOutlet weak var artistTableview: UITableView!
    
    var arrData = [JsonModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
       jsonParsing()
    }

    func jsonParsing(){
        let url = URL(string:"https://itunes.apple.com/search?media=music&term=bollywood")
        URLSession.shared.dataTask(with: url!) { (data, response, error)
            in
            guard let data = data else { return }
            do {
                let json = try JSON(data:data)
                let results = json["results"];
                for arr in results.arrayValue{
                    //print(arr["trackPrice"])
                    self.arrData.append(JsonModel(json: arr))
                }
                DispatchQueue.main.async {
                self.artistTableview.reloadData()
                }
            print(self.arrData)
            }catch{
                print(error.localizedDescription)
            }
        }.resume()
    }
}
extension ViewController:UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath) as! TableViewCell
        cell.labelArtist.text = arrData[indexPath.row].artistName
        cell.labelTrack.text = arrData[indexPath.row].trackCensoredName
        
        let url = URL(string: arrData[indexPath.row].artworkUrl100)
        
        cell.img.kf.setImage(with: url)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

}
