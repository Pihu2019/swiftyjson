//
//  TableViewCell.swift
//  SwiftyJson
//
//  Created by PRIYA GONGAL on 29/07/19.
//  Copyright © 2019 Priya Gongal. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var labelArtist: UILabel!
    @IBOutlet weak var labelTrack: UILabel!
    
    @IBOutlet weak var img: UIImageView!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
