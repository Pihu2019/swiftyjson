//
//  JsonModel.swift
//  SwiftyJson
//
//  Created by PRIYA GONGAL on 29/07/19.
//  Copyright © 2019 Priya Gongal. All rights reserved.
//

import Foundation

struct JsonModel {
    var artistName: String = ""
    var trackCensoredName: String = ""
    var artworkUrl100: String = ""
    init() {
        
    }
    init(json:JSON) {
        artistName = json["artistName"].stringValue
        trackCensoredName = json["trackCensoredName"].stringValue
        artworkUrl100 = json["artworkUrl100"].stringValue
    }
}
